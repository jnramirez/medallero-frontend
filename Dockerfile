  FROM jeremiasfiuba/node-chrome:v1
  WORKDIR /usr/src/app 
  COPY package.json package-lock.json ./ 
  RUN npm ci 
  COPY . . 
  ENV CHROME_BIN=/usr/bin/chromium
  ENTRYPOINT npm run test:ci
