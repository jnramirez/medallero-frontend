import { inject } from "@angular/core";
import { CanActivateFn } from "@angular/router";
import { AuthService } from "./auth.service";

export function authenticationGuard(): CanActivateFn {
    return () => {
        const authService: AuthService = inject(AuthService);
        return authService.hasAccess();
    };
}