import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RegisteredUser } from '../models/registeredUser';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  login(email: string, _password: string): Observable<RegisteredUser> {
    return new Observable((observer) => {
      const registeredUser: RegisteredUser = {
        email,
        firstName: 'Juan',
        lastName: 'Perez',
        birth: new Date(),
        role: 'USER',
        token: '',
      };
      localStorage.setItem('mockSession', JSON.stringify(registeredUser));
      observer.next(registeredUser);
    });
  }

  logout(): void {
    localStorage.removeItem('mockSession');
  }

  hasAccess(): boolean {
    return !!localStorage.getItem('mockSession');
  }
}
