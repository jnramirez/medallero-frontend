import { User } from "./user";

type Role = 'USER' | 'ADMIN';

export interface RegisteredUser extends User {
    token: string;
    role: Role;
}