import { NgModule } from '@angular/core';
import { NgFor, NgIf } from '@angular/common';

import { RouterModule } from '@angular/router';
import { PanelComponent } from './panel.component';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatDividerModule } from '@angular/material/divider';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  imports: [
    RouterModule,
    NgIf,
    NgFor,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatSidenavModule,
    MatDividerModule,
    BrowserAnimationsModule,
  ],
  declarations: [PanelComponent],
})
export class PanelModule { }