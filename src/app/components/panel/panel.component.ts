import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';

interface MenuItem {
  id: string;
  path: string;
  text: string;
  icon: string;
}

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.scss']
})
export class PanelComponent {

  constructor(private router: Router, private authService: AuthService) { }

  menuItems: MenuItem[] = [
    { id: 'home', path: '/home', text: 'Home', icon: 'home' },
    { id: 'profile', path: '/profile', text: 'My Profile', icon: 'person' },
    { id: 'showcase', path: '/showcase', text: 'Showcase', icon: 'workspace_premium' },
    { id: 'upload', path: '/upload', text: 'Upload Data', icon: 'cloud_upload' },
  ];

  isActive(item: MenuItem): boolean {
    return item.path === this.router.url;
  }

  logout() {
    this.authService.logout();
    this.router.navigateByUrl('/login');
  }

}
